(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, blackbox) {

            $scope.run = function() {
                blackbox.yahoo()
                    .then(function(data) {
                        console.log('data', data);
                    });
            };

        });
})();
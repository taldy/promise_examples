(function() {

    angular
        .module('app')
        .factory('blackbox', function($q) {
            return {
                yahoo: function() {
                    var deferred = $q.defer();

                    deferred.resolve(Math.random());

                    return deferred.promise;
                }
            };
        })
})();
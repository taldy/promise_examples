(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            function hafanana() {
                var deferred = $q.defer();

                deferred.resolve(parseInt($scope.value));

                return deferred.promise;
            }

            $scope.caramba = function() {
                hafanana()
                    .then(function(data) {
                        console.log('Resolved with reason', data);

                        data++;
                        return data;
                    })
                    .then(function(data) {
                        console.log('Second .then', data);

                        data *= 2;
                        return data;
                    })
                    .then(function(data) {
                        console.log('Third', data);
                    })
                    .then(function(data) {
                        console.log('One more', data);
                    });
            }

        });
})();
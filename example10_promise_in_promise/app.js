(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $http) {

            $scope.index = 0;

            $scope.run = function() {

                $http.get('http://jsonplaceholder.typicode.com/posts')
                    .then(function(response) {

                        console.log('Posts', response.data);
                        var postId = response.data[$scope.index].id;

                        return $http.get('http://jsonplaceholder.typicode.com/posts/' + postId)
                    })
                    .then(function(response) {
                        console.log('Post #', $scope.index, response.data);
                    });
            };

        });
})();
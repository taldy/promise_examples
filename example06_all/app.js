(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            var oldDeferred = $q.defer();
            oldDeferred.resolve(new Date());

            function bugagashenka(param) {
                switch (param) {
                    case 'value':
                        return 77;
                    case 'promiseInFuture':
                        var deferred = $q.defer();

                        setTimeout(function() {
                            deferred.resolve(Math.random());
                        }, 3000);

                        return deferred.promise;
                    case 'oldPromise':
                        return oldDeferred.promise;
                }
            }

            $scope.run = function() {

                var promises = [];
                promises.push($q.when(bugagashenka('value')));
                promises.push($q.when(bugagashenka('promiseInFuture')));
                promises.push($q.when(bugagashenka('oldPromise')));

                $q.all(promises)
                    .then(function(data) {
                        console.log('All promises data', data);

                        return data;
                    });
            };


        });
})();
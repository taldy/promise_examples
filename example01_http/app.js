(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $http) {

            $scope.url = 'http://jsonplaceholder.typicode.com/posts';

            $scope.run = function() {

                $http.get($scope.url)
                    .success(function(data, status, headers, config) {
                        console.log('Success', status, data);
                    })
                    .error(function(data, status, headers, config) {
                        console.log('Error', status, data);
                    });
            };

            $scope.run2 = function() {

                $http.get($scope.url)
                    .then(function(response) {
                        console.log('Response', response);
                    }, function(response) {
                        console.log('Error', response);
                    });
            };

        });
})();
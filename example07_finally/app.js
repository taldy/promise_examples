(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            function expeliarmos() {
                var deferred = $q.defer();

                var value = Math.random();

                if (value > 0.5) {
                    deferred.resolve(value);
                }
                else {
                    deferred.reject(value);
                }

                return deferred.promise;
            }

            $scope.run = function() {

                expeliarmos()
                    .then(function(data) {
                        console.log('Resolve', data);

                        return data;
                    }, function(reason) {
                        console.log('Reject', reason);

                        return $q.reject(reason);
                    })
                    .finally(function(data) {
                        console.log('Finally', data);
                    });
            };

            $scope.run2 = function() {

                expeliarmos()
                    .finally(function(data) {
                        console.log('Finally', data);
                        return 'new data';
                    })
                    .then(function(data) {
                        console.log('Resolve', data);

                        return data;
                    }, function(reason) {
                        console.log('Reject', reason);

                        return $q.reject(reason);
                    });
            };

            $scope.run3 = function() {

                expeliarmos()
                    .finally(function(data) {
                        console.log('Finally', data);
                        return $q.reject('hahaha');
                    })
                    .then(function(data) {
                        console.log('Resolve', data);

                        return data;
                    }, function(reason) {
                        console.log('Reject', reason);

                        return $q.reject(reason);
                    });
            };

        });
})();
(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            function apolonegra() {
                var deferred = $q.defer();

                var value = Math.random();
                console.log('value', value);

                if (value > 0.5) {
                    deferred.resolve(value);
                }
                else {
                    deferred.reject(value);
                }

                return deferred.promise;
            }

            $scope.run = function() {

                apolonegra()
                    .then(null, function(reason) {
                        console.log('Reject', reason);
                    });
            };

            $scope.run2 = function() {

                apolonegra()
                    .catch(function(reason) {
                        console.log('Catch', reason);
                    });
            };

        });
})();
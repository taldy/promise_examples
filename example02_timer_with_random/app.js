(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            function hafanana() {
                var deferred = $q.defer();

                setTimeout(function() {
                    var value = Math.random();

                    if (value > 0.5) {
                        deferred.resolve(value);
                    }
                    else {
                        deferred.reject(value);
                    }

                }, 3000);

                return deferred.promise;
            }

            $scope.run = function() {
                hafanana()
                    .then(function(data) {
                        console.log('Resolved with reason', data);
                    }, function(data) {
                        console.log('Rejected with reason', data);
                    });
            };
        });
})();
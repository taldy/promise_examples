(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            var deferred;
            $scope.reason = '';


            function hafanana() {
                deferred = $q.defer();

                return deferred.promise;
            }


            $scope.resolve = function() {
                deferred.resolve($scope.reason);
            };
            $scope.reject = function() {
                deferred.reject($scope.reason);
            };


            hafanana()
                .then(function(data) {
                    console.log('Resolved with reason', data);
                }, function(data) {
                    console.log('Rejected with reason', data);
                });

        });
})();
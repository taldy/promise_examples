(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            function someThirdPartyTool() {
                var deferred = $q.defer();

                deferred.resolve(Math.random());

                return deferred.promise;
            }

            function bbBbBbbebeebe(){
                return someThirdPartyTool()
                    .then(function(data) {

                        console.log('First resolve', data);

                        return data > 0.5 ? data : $q.reject('value ' + data + ' is to small');
                    });
            }

            $scope.run = function() {

                bbBbBbbebeebe()
                    .then(function(data) {
                        console.log('Second resolve', data);
                        return data;
                    }, function(data) {
                        console.log('Reject', data);
                        return $q.reject(data);
                    })
                    .then(function(data) {
                        console.log('Resolve #3', data);
                    }, function(data) {
                        console.log('Reject #3', data);
                    })
                    .then(function(data) {
                        console.log('Resolve #4', data);
                    }, function(data) {
                        console.log('Reject #4', data);
                    })
            };


        });
})();
(function() {
    'use strict';

    angular
        .module('app', [])
        .controller('AppController', function($scope, $q) {

            var lastPromise;
            $scope.kindOfPromise = 'value';

            var oldDeferred = $q.defer();
            oldDeferred.resolve(new Date());

            function bugagashenka(param) {
                switch (param) {
                    case 'value':
                        return 77;
                    case 'promiseInFuture':
                        var deferred = $q.defer();

                        setTimeout(function() {
                            deferred.resolve(Math.random());
                        }, 3000);

                        return deferred.promise;
                    case 'oldPromise':
                        return oldDeferred.promise;
                }
            }

            $scope.run = function() {
                lastPromise = $q.when(bugagashenka($scope.kindOfPromise))
                    .then(function(data) {
                        console.log(data);

                        return data;
                    });
            };

            $scope.esche = function() {
                lastPromise
                    .then(function(data) {
                        console.log('   #2', data);
                    });
            }


        });
})();